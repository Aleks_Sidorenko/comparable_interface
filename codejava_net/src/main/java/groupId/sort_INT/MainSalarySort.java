package groupId.sort_INT;

import java.util.Arrays;

/**
 * Created on 25.05.2021 15:50.
 *
 * @author Aleks Sidorenko (e-mail: alek.sidorenko@gmail.com).
 * @version Id$.
 * @since 0.1.
 */
public class MainSalarySort {

    public static void main(String[] args) {
        Employee[] employees = new Employee[4];

        employees[0] = new Employee("Tom", 45, 80000);
        employees[1] = new Employee("Sam", 56, 75000);
        employees[2] = new Employee("Alex", 30, 120000);
        employees[3] = new Employee("Peter", 25, 60000);

        //System.out.println("Before sorting: " + Arrays.toString(employees));
        System.out.println("Before sorting: ");
        for (Employee e: employees) {
            System.out.println("Name=" + e.getName() + ", age=" + e.getAge() + ", salary=" + e.getSalary());
        }
        Arrays.sort(employees);
        //System.out.println("After sorting: " + Arrays.toString(employees));
        System.out.println("After sorting: ");
        for (Employee e: employees) {
            System.out.println("Name=" + e.getName() + ", age=" + e.getAge() + ", salary=" + e.getSalary());
        }
    }
}
